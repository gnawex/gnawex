# GNAWEX

An independent marketplace for [MouseHunt](https://mousehuntgame.com).

**NOTE:** This project is still under active development; expect a lot of
breaking changes.

## Documentation

- [docs.gnawex.com](https://docs.gnawex.com) - Project's user guide

